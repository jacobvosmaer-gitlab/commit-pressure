# Put commit pressure on a Git repository

Preparation.

```shell
git clone --bare https://gitlab.com/gitlab-org/gitlab-test.git
cd gitlab-test.git
git config core.logAllRefUpdates true
git config gc.auto 0

# Create 10000 branches
seq -f 'create refs/heads/branch-%g HEAD' 10000 | git update-ref --stdin
```

Now run `increment-commit-sequence` in that repository and it will
create commits on master in a loop. If you run more than one instance
of `increment-commit-sequence` some of the ref updates will fail but
the loop will keep running.

## Extra mayhem

Run `git pack-refs` in a loop: use the `--pack-refs` option when invoking `increment-commit-sequence`
