#!/usr/bin/env ruby
require 'etc'
require 'logger'
require 'optparse'
require 'open3'
require 'tempfile'

REF = 'refs/heads/master'
PROGNAME = 'increment-commit-sequence'
EMPTY_PACKFILE = [
  0x50, 0x41, 0x43, 0x4b, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x02, 0x9d, 0x08, 0x82,
  0x3b, 0xd8, 0xa8, 0xea, 0xb5, 0x10, 0xad, 0x6a, 0xc7, 0x5c, 0x82, 0x3c, 0xfd, 0x3e, 0xd3, 0x1e,
].pack('C*')
PKT_FLUSH = '0000'
ERROR_FILE = PROGNAME + '.error'
LOGGER = Logger.new(STDERR, progname: PROGNAME)

def main(options)
  options[:sleep] ||= 0

  if options[:pack_refs]
    Thread.new do
      loop do
        system(*%w[git pack-refs --all])
        LOGGER.info("git pack-refs: #{$?}")
        sleep(1)
      end
    end
  end

  test = Test.new
  loop do
    abort "found #{ERROR_FILE}; exiting" if test.error?

    test.increment_sequence
    sleep(options[:sleep])
  end
end

class Test
  def increment_sequence
    Open3.popen2(*%w[git receive-pack .]) do |stdin, stdout, wait_thr|
      stdin.binmode
      stdout.binmode

      advertised_refs = {}
      loop do
        pkt = read_pkt(stdout)
        unless pkt
          LOGGER.error('short read during ref advertisement')
          return
        end

        break if pkt == PKT_FLUSH

        pkt = pkt.split("\x00", 2)[0] # Strip capability advertisement
        pkt.chomp!
        pkt = pkt[4, pkt.size]

        split = pkt.split(' ', 2)
        unless split.size == 2
          LOGGER.error "invalid advertisement packet"
          return
        end

        advertised_refs[split[1]] = split[0]
      end

      current_commit = advertised_refs[REF]
      unless current_commit
        LOGGER.error("missing #{REF} in ref advertisement")
        return
      end

      if @previously_seen_commit
        args = %W[git merge-base --is-ancestor #{@previously_seen_commit} #{current_commit} ]
        _, ok = capture(args)

        unless ok
          fatal("previously seen commit #{@previously_seen_commit} is not an ancestor of current #{REF} #{current_commit}")
        end
      end

      @previously_seen_commit = current_commit

      commit_args = %W[git commit-tree -p #{current_commit} -m #{commit_message} #{current_commit}^{tree}]
      new_commit, ok = capture(commit_args)
      unless ok
        fatal('could not create new commit')
      end

      new_commit.chomp!

      write_pkt(stdin, "#{current_commit} #{new_commit} #{REF}\n")
      stdin.write(PKT_FLUSH)
      stdin.write(EMPTY_PACKFILE)
      stdin.close

      # Consume and print status information from the server, if any
      loop do
        pkt = read_pkt(stdout)
        break unless pkt
        p pkt
      end

      LOGGER.info("git receive-pack: #{wait_thr.value}")
    end
  end

  def commit_message
    "#{Etc.uname[:nodename]} #{Process.pid} #{Time.now.to_f}"
  end
  
  def fatal(msg)
    LOGGER.fatal(msg)
    
    Tempfile.open(PROGNAME, '.') do |f|
      f.puts commit_message
      f.puts msg
      f.close
      File.link(f.path, ERROR_FILE)
    end
    
    abort
  end

  def error?
    File.exist?(ERROR_FILE)
  end
end

def capture(cmd)
  out = IO.popen(cmd, &:read)
  [out, $?.success?]
end

def read_pkt(io)
  header = io.read(4)
  return unless header

  size = Integer('0x'+header)
  return header if size < 4

  data = io.read(size - 4)
  return unless data

  header + data
end

def write_pkt(io, data)
  io.printf("%04x%s", data.size + 4, data)
end

options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: #{PROGNAME} [options]"

  opts.on("-sSLEEP_TIME", "--sleep=SLEEP_TIME", Integer, "Sleep in main loop, in seconds") do |s|
    options[:sleep] = s
  end

  opts.on("-CDIR", "", "Change working directory") do |dir|
    Dir.chdir(dir)
  end

  opts.on("", "--pack-refs", "Also run pack-refs in a loop") do
    options[:pack_refs] = true
  end
end.parse!

main(options)
